$(document).ready(function(){

  // load video into overlay
  $('.video-thumb').click(function(){

    var video_url = $(this).data('video');
    var video_overlay = $('.video-overlay');
    var video = video_overlay.find('video').get(0);

    // add video url to video element
    $('.video-overlay video source').attr('src', video_url);

    // reload video
    video.load();

    // fade in overlay
    video_overlay.fadeIn();

    // play video
    video.play();

    // close when the video finishes playing
    video.onended = function(){
      video_overlay.fadeOut();
    };

  });

});

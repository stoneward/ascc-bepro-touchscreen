var config = {
  "export_path" : "public/assets/",
  "tasks" : ['sass', 'bootstrap-js-sync', 'bootstrap-font-sync'],
  "node_modules_path" : './node_modules'
};

var gulp = require('gulp'),
    sass = require('gulp-ruby-sass'),
    minifycss = require('gulp-minify-css'),
    rename = require('gulp-rename'),
    watch = require('gulp-watch'),
    notify = require('gulp-notify'),
    autoprefixer = require('gulp-autoprefixer');

gulp.task('default', config.tasks, function() {
  gulp.watch('assets/sass/**/*.scss', ['sass']);
});

gulp.task('sass', function () {
  return sass('assets/sass/app.scss',{
    loadPath: [
        config.node_modules_path + '/bootstrap-sass/assets/stylesheets',
    ]})
    .on('error', sass.logError)
    .pipe(autoprefixer({
      browsers: ['last 3 versions'],
      cascade: false
    }))
    .pipe(gulp.dest(config.export_path + 'css'))
		.pipe(rename({ suffix: '.min' }))
		.pipe(minifycss())
		.pipe(gulp.dest(config.export_path + 'css'))
		.pipe(notify('SCSS Compiled.'));
});

// sync latest version of bootstrap from npm directory
gulp.task('bootstrap-js-sync', function(){
  return gulp.src(config.node_modules_path + '/bootstrap-sass/assets/javascripts/bootstrap.min.js')
    .pipe(gulp.dest(config.export_path + 'js'));
});
gulp.task('bootstrap-font-sync', function(){
  return gulp.src(config.node_modules_path + '/bootstrap-sass/assets/fonts/bootstrap/*.*')
    .pipe(gulp.dest(config.export_path + 'fonts/bootstrap'));
});
